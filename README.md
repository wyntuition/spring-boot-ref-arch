# spring-boot-ref-arch

*Reference architecture for a reactive Spring Boot app, using Reactor*

The app is configured with the follow, so needs a database:

* Reactor for reactive functional programming
* Spring Data JDBC
* Flyway for Database Migrations
* PostgreSQL in a container for local development
* Lombok
* Checkstyle
* Spring Actuator
* JUnit

## Running 

1. To run a database for the application (which is configured for PostgreSQL), run `docker-compose up -d` which will start it in a container that the app will talk to.

1. Run `gradle bootRun` and when it builds and runs, navigate to `http://localhost:9301`
