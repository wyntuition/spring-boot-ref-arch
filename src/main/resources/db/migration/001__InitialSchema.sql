CREATE TABLE Person (
  id SERIAL PRIMARY KEY,
  firstname VARCHAR(30),
  lastname VARCHAR(30),
  userfield VARCHAR(40),
  timestamp TIMESTAMP DEFAULT NOW(),
  submitted_by VARCHAR(8)
)
