package com.wyn.dev.demo.controllers;

import com.wyn.dev.demo.models.Person;
import com.wyn.dev.demo.services.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class ExampleController {

  @Autowired
  private PersonService personService;

  @RequestMapping("/")
  public Mono<Person> hello(String logonId) {
    return personService.getPerson(logonId);
  }
}
