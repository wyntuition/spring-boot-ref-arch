package com.wyn.dev.demo.services;

import com.wyn.dev.demo.models.Person;

import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class PersonService {
    
  public PersonService () { }
  
  public Mono<Person> getPerson(String logonId) {
    return Mono.just(new Person());
  }
}